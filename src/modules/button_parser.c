#include "json.h"
#include "json.c"
#include "base64.h"
#include "base64.c"

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

// #include "base64.c"


int mode;
int sinr;
char* data_parse;
json_value* section;


static void process_value(json_value* value, int depth);

static void process_object(json_value* value, int depth)
{
        int length, x;
        if (value == NULL) {
                return;
        }

        length = value->u.object.length;
        for (x = 0; x < length; x++) {
                if (strcmp(value->u.object.values[x].name, "data") == 0){
                        section = value->u.object.values[x].value;
                        data_parse = section->u.string.ptr;
                } else if (strcmp(value->u.object.values[x].name, "sinr") == 0){
                        section = value->u.object.values[x].value;
                        sinr = section->u.dbl;
                }

                process_value(value->u.object.values[x].value, depth+1);
        }
}

static void process_value(json_value* value, int depth)
{
        if (value == NULL) {
                return;
        }
        switch (value->type) {
                case json_object:
                        process_object(value, depth+1);
                        break;
                case json_string:
                        if (strcmp(value->u.string.ptr, "click") == 0){
                                mode = 1;
                        } else if (strcmp(value->u.string.ptr, "double_click") == 0){
                                mode = 2;
                        }else {
                                mode = 3;
                        }
                        break;
        }
}

static int press_mode(char* parsing_str){
        json_char* json = (json_char*)parsing_str;
        int file_size = strlen(parsing_str);

        json_value* value = json_parse(json,file_size);

        if (value == NULL) {
                fprintf(stderr, "Unable to parse data\n");
                exit(1);
        }

        process_value(value, 0);
        if (sinr < 0){
                mode = sinr;
        }

        return mode;
}

static char* parse(char* parsing_str){
        json_char* json = (json_char*)parsing_str;
        int file_size = strlen(parsing_str);

        json_value* value = json_parse(json,file_size);

        if (value == NULL) {
                fprintf(stderr, "Unable to parse data\n");
                exit(1);
        }

        process_value(value, 0);

        return data_parse;
}

// int main(void)
// {
//         char* filename;
//         FILE *fp;
//         struct stat filestatus;
//         int file_size;
//         char* file_contents;
//         json_char* json;
//         json_value* value;

//         filename = "test.json";
//         if ( stat(filename, &filestatus) != 0) {
//                 fprintf(stderr, "File %s not found\n", filename);
//                 return 1;
//         }
//         file_size = filestatus.st_size;
//         file_contents = (char*)malloc(filestatus.st_size);
//         if ( file_contents == NULL) {
//                 fprintf(stderr, "Memory error: unable to allocate %d bytes\n", file_size);
//                 return 1;
//         }

//         fp = fopen(filename, "rt");
//         if (fp == NULL) {
//                 fprintf(stderr, "Unable to open %s\n", filename);
//                 fclose(fp);
//                 free(file_contents);
//                 return 1;
//         }
//         if ( fread(file_contents, file_size, 1, fp) != 1 ) {
//                 fprintf(stderr, "Unable to read content of %s\n", filename);
//                 fclose(fp);
//                 free(file_contents);
//                 return 1;
//         }
//         fclose(fp);

//         json = (json_char*)file_contents;

//         value = json_parse(json,file_size);

//         if (value == NULL) {
//                 fprintf(stderr, "Unable to parse data\n");
//                 free(file_contents);
//                 exit(1);
//         }

//         process_value(value, 0);

//         //printf("mode: %d", press_mod(value));

//         //press_mod()

//         json_value_free(value);
//         free(file_contents);
//         return 0;
// }


int main(void) {

        char* filename;
        FILE *fp;
        struct stat filestatus;
        int file_size;
        char* file_contents;
        json_char* json;
        json_value* value;

        filename = "input.json";

        if ( stat(filename, &filestatus) != 0) {
                fprintf(stderr, "File %s not found\n", filename);
                return 1;
        }
        file_size = filestatus.st_size;
        file_contents = (char*)malloc(filestatus.st_size);
        if ( file_contents == NULL) {
                fprintf(stderr, "Memory error: unable to allocate %d bytes\n", file_size);
                return 1;
        }

        fp = fopen(filename, "rt");
        if (fp == NULL) {
                fprintf(stderr, "Unable to open %s\n", filename);
                fclose(fp);
                free(file_contents);
                return 1;
        }
        if ( fread(file_contents, file_size, 1, fp) != 1 ) {
                fprintf(stderr, "Unable to read content of %s\n", filename);
                fclose(fp);
                free(file_contents);
                return 1;
        }
        fclose(fp);


        json = (json_char*)file_contents;

        value = json_parse(json,file_size);

        if (value == NULL) {
                fprintf(stderr, "Unable to parse data\n");
                free(file_contents);
                exit(1);
        }

        process_value(value, 0);

        json_value_free(value);
        free(file_contents);

        char* buf64 = "out.json";

        FILE* out = fopen(buf64, "rw");

	size_t* len_out;
	char* decs = base64_decode(data_parse, strlen(data_parse), len_out);
        fputs(decs, out);

        fclose(out);


        filename = buf64;

        if ( stat(filename, &filestatus) != 0) {
                fprintf(stderr, "File %s not found\n", filename);
                return 1;
        }
        file_size = filestatus.st_size;
        file_contents = (char*)malloc(filestatus.st_size);
        if ( file_contents == NULL) {
                fprintf(stderr, "Memory error: unable to allocate %d bytes\n", file_size);
                return 1;
        }

        fp = fopen(filename, "rt");
        if (fp == NULL) {
                fprintf(stderr, "Unable to open %s\n", filename);
                fclose(fp);
                free(file_contents);
                return 1;
        }
        if ( fread(file_contents, file_size, 1, fp) != 1 ) {
                fprintf(stderr, "Unable to read content of %s\n", filename);
                fclose(fp);
                free(file_contents);
                return 1;
        }
        fclose(fp);


        json = (json_char*)file_contents;

        value = json_parse(json,file_size);

        if (value == NULL) {
                fprintf(stderr, "Unable to parse data\n");
                free(file_contents);
                exit(1);
        }

        process_value(value, 0);

        FILE* fileUser = fopen("button_mod.txt", "w");

        fprintf(fileUser, "%d %d", mode, sinr);

        json_value_free(value);
        free(file_contents);
        fclose(fileUser);

	return 0;
}



