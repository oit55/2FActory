from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, SubmitField
from wtforms.validators import DataRequired

class TypeForm(FlaskForm):
	title = StringField('OIT-55')
	information = StringField('Sequence of commands')
	max_wait_time = StringField('Max waiting time')
	types = SelectField('command', choices=[(1, 'single'), (2, 'double'), (3, 'long')])

	submit = SubmitField('Add command')
	refresh = SubmitField('Refresh')
	save = SubmitField('Save configuration')

	def save_type(self, types_dict, types_cnt):
		types_dict[types_cnt] = self.types

class LoginForm(FlaskForm):
	login = StringField('Login', validators=[DataRequired()])
	submit = SubmitField('Submit')